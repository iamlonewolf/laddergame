export class Player {
  id?: number;
  name?: string;
  lineColor?: string;
  emoji?: string;
}
