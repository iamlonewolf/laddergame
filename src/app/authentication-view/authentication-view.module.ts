import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationViewRouteModule } from "./authentication-view-route.module";
import { AuthenticationViewComponent } from "./authentication-view.component";
import { AuthenticationAppModule } from "../shared/authentication-app/authentication-app.module";

@NgModule({
    imports: [
        NgbModule,
        FormsModule,
        AuthenticationAppModule,
        AuthenticationViewRouteModule,
    ],
    declarations: [
        AuthenticationViewComponent
    ],
    providers: [
    ],
})
export class AuthenticationViewModule {}
