import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PurchaseViewRouteModule } from "./purchase-view-route.module";

@NgModule({
  declarations: [
  ],
  imports: [
    NgbModule,
    FormsModule,
    PurchaseViewRouteModule
  ],
  providers: [
  ],
})
export class PurchaseViewModule {}
