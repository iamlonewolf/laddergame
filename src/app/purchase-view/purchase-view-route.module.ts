import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PurchaseViewComponent } from "./purchase-view.component";

const routes: Routes = [
  {
    path: "",
    component: PurchaseViewComponent
  }
];
@NgModule({
  declarations: [
    PurchaseViewComponent
  ],
  imports: [
    RouterModule.forChild(routes)
  ],
})
export class PurchaseViewRouteModule {}
