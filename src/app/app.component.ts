import { Component } from "@angular/core";
import {
    ActivatedRoute,
    Router,
    NavigationStart,
    NavigationEnd,
    RouterEvent
} from "@angular/router";
@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: [
        "./app.component.scss"
    ]
})
export class AppComponent {
    private loading: boolean = false;
    private navCanvasOpen: boolean = false;
    constructor(private route: ActivatedRoute,
        private router: Router) {

        this.router
            .events
            .subscribe((event: RouterEvent) => {
                if (event instanceof NavigationStart) {
                    this.setLoading(true);
                    console.log("START NAVIGATION");
                }

                if (event instanceof NavigationEnd) {
                    this.setLoading(false);
                    this.navCanvasOpen = false;
                    console.log("END NAVIGATION");
                }
            });
    }

    toggle(): void {
        this.navCanvasOpen = !this.navCanvasOpen;
    }

    isNavCanvasOpen(): boolean {
        return this.navCanvasOpen;
    }

    open(): void {
    }

    isLoading(): boolean {
        return this.loading;
    }

    setLoading(loading: boolean): void {
        this.loading = loading;
    }

    onActivate(): void {
        console.log("ON ACTIVATE");
    }

    onDeactivate(): void {
        console.log("ON DEACTIVATE");

    }

}
