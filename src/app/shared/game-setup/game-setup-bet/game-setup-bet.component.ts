import { Component, ElementRef, ViewChild } from "@angular/core";
import { GameSetupService } from "../game-setup.service";
import { Player } from "../../../model/types/player";

@Component({
    selector: "app-game-setup-bet",
    templateUrl: "./game-setup-bet.component.html",
    styleUrls: [
        "./game-setup-bet.component.scss"
    ]
})
export class GameSetupBetComponent {
    constructor(private gameSetupService: GameSetupService) {
    }

    getList(): Player[] {
        return this.gameSetupService.getPlayers();
    }

    private reset(): void {
    }
}
