import { Component, ElementRef, ViewChild } from "@angular/core";
import { GameSetupService } from "./game-setup.service";
import { Player } from "../../model/types/player";
import { GameSetupStateService } from "./game-setup-state.service";
import { RungGeneratorService } from '../rung-generator/rung-generator.service';

@Component({
    selector: "app-game-setup",
    templateUrl: "./game-setup.component.html",
    styleUrls: [
        "./game-setup.component.scss"
    ]
})
export class GameSetupComponent {
    constructor(private gameSetupStateService: GameSetupStateService,
                private gameSetupService: GameSetupService,
                private rungGeneratorService: RungGeneratorService) {
        this.gameSetupService.setDebugMode(true);
    }

    getTotalPlayers(): number {
        return this.gameSetupService.getTotalPlayers();
    }

    isStepPlayer(): boolean {
        return this.gameSetupStateService.isStepPlayer();
    }

    isStepBet(): boolean {
        return this.gameSetupStateService.isStepBet();
    }

    isStepGenerateRungs(): boolean {
        return this.gameSetupStateService.isStepGenerateRungs();
    }

    goNext(): void {
        const currentStep = this.gameSetupStateService.getCurrentStep();
        this.gameSetupStateService.goNextStep(currentStep + 1);
    }

    goBack(): void {
        const currentStep = this.gameSetupStateService.getCurrentStep();
        this.gameSetupStateService.goNextStep(currentStep - 1);
    }

    getCurrentStep(): number {
        return this.gameSetupStateService.getCurrentStep();
    }

    draw(): void {
        this.rungGeneratorService.draw();
    }

    isDebugMode(): boolean {
        return this.gameSetupService.isDebugMode();
    }
}
