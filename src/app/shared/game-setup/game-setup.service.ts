import * as _ from "lodash";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { Player } from "../../model/types/player";
@Injectable()
export class GameSetupService {
    static readonly ERROR_MISSING_REQUIRED_PARAMETERS: string = "MISSING_REQUIRED_PARAMETERS";
    private debug: boolean = false;
    private players: Player[] = [];
    constructor() {
    }

    addPlayer(player: Player): Observable<Player[]> {
        if (!_.has(player, "name")) {
            return throwError(GameSetupService.ERROR_MISSING_REQUIRED_PARAMETERS);
        }
        player.id = _.get(_.last(this.getPlayers()), "id", 0) + 1;
        this.players.push(player);
        console.log("New player added: "
            + " Name: " + player.name
            + " Rung color: ", player.lineColor
            + " Player List: ", this.getPlayers());
        return of(this.players);
    }

    removePlayer(id: number): Observable<Player[]> {
        _.remove(this.players, {
            id: id
        });
        return of(this.players);
    }

    getTotalPlayers(): number {
        return _.size(this.getPlayers());
    }

    getPlayers(): Player[] {
        return this.players;
    }

    getPlayersObservable(): Observable<Player[]> {
        return of(this.getPlayers());
    }

    getDataToString(): string {
        return JSON.stringify(this.getPlayers());
    }

    setDebugMode(debug: boolean = false): void {
        this.debug = debug;
    }

    isDebugMode(): boolean {
        return this.debug;
    }
}


