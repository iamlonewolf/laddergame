import * as _ from "lodash";
import { Component, ElementRef, ViewChild, OnInit } from "@angular/core";
import { GameSetupService } from "../game-setup.service";
import { Player } from "../../../model/types/player";

@Component({
  selector: "app-game-setup-player",
  templateUrl: "./game-setup-player.component.html",
  styleUrls: [
    "./game-setup-player.component.scss"
  ]
})
export class GameSetupPlayerComponent implements OnInit {
    @ViewChild("currentNameInput", {static: false}) currentNameInput: ElementRef;

    private currentColor: string;
    private currentName: string;
    constructor(private gameSetupService: GameSetupService) {
        this.gameSetupService.setDebugMode(true);
        this.initialize();
    }

    private initialize(): void {
        // Minimum player should be 2
        // _.map(_.range(0, 2), (index) => {
        //     const player = new Player();
        //     player.id = index;
        //     player.name = "";
        //     this.gameSetupService.addPlayer(player).subscribe();
        // });
    }

    private reset(): void {
        this.currentName = "";
        this.currentColor = "";
        this.currentNameInput.nativeElement.focus();
    }

    getPlayers(): Player[] {
        return this.gameSetupService.getPlayers();
    }

    getTotalPlayers(): number {
        return this.gameSetupService.getTotalPlayers();
    }

    addPlayer(): void {
        this.gameSetupService.addPlayer({
            name: this.getCurrentName(),
            lineColor: this.getCurrentColor()
        }).subscribe(() => {
            this.reset();
        }, error => {
            console.error("ERROR", error);
        });
    }

    removePlayer(player: Player): void {
        this.gameSetupService.removePlayer(player.id)
        .subscribe();
    }

    getCurrentColor(): string {
        return this.currentColor;
    }

    getCurrentName(): string {
        return this.currentName;
    }

    getAttributeName(namespace: string, name: string): string {
        return namespace + "-" + name;
    }

    isDebugMode(): boolean {
        return this.gameSetupService.isDebugMode();
    }

    getDataToString(): string {
        return this.gameSetupService.getDataToString();
    }

    ngOnInit() {

    }
}
