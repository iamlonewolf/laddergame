import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ColorPickerModule } from "ngx-color-picker";
import { EmojiPickerModule } from "../emoji-picker/emoji-picker.module";
import { GameSetupService } from "./game-setup.service";
import { GameSetupStateService } from "./game-setup-state.service";
import { GameSetupDebugComponent } from "./game-setup-debug/game-setup-debug.component";
import { GameSetupComponent } from "./game-setup.component";
import { GameSetupPlayerComponent } from "./game-setup-player/game-setup-player.component";
import { GameSetupBetComponent } from "./game-setup-bet/game-setup-bet.component";
import { GameSetupRungComponent } from "./game-setup-rung/game-setup-rung.component";
import { RungGeneratorModule } from "../rung-generator/rung-generator.module";

@NgModule({
    imports: [
        NgbModule,
        CommonModule,
        FormsModule,
        ColorPickerModule,
        EmojiPickerModule,
        RungGeneratorModule,
    ],
    declarations: [
        GameSetupComponent,
        GameSetupPlayerComponent,
        GameSetupBetComponent,
        GameSetupRungComponent,
        GameSetupDebugComponent
    ],
    exports: [
        GameSetupComponent,
        GameSetupPlayerComponent,
        GameSetupBetComponent,
        GameSetupRungComponent,
        GameSetupDebugComponent
    ],
    providers: [
        GameSetupService,
        GameSetupStateService
    ]
})
export class GameSetupModule {}
