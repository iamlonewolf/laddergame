import { Component, ElementRef, ViewChild } from "@angular/core";
import { GameSetupService } from "../game-setup.service";

@Component({
    selector: "app-game-setup-debug",
    templateUrl: "./game-setup-debug.component.html",
    styleUrls: [
        "./game-setup-debug.component.scss"
    ]
})
export class GameSetupDebugComponent {
    constructor(private gameSetupService: GameSetupService) {
    }

    getTotalPlayers(): number {
        return this.gameSetupService.getTotalPlayers();
    }

    getDataToString(): string {
        return this.gameSetupService.getDataToString();
    }
}
