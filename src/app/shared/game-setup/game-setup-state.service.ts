import { Injectable } from "@angular/core";
@Injectable()
export class GameSetupStateService {
    static readonly STEP_PLAYER: number = 0;
    static readonly STEP_BET: number = 1;
    static readonly STEP_GENERATE_RUNGS: number = 2;

    private currentStep: number = GameSetupStateService.STEP_PLAYER;

    constructor() {
    }

    isStepPlayer(): boolean {
        return this.currentStep === GameSetupStateService.STEP_PLAYER;
    }

    isStepBet(): boolean {
        return this.currentStep === GameSetupStateService.STEP_BET;
    }

    isStepGenerateRungs(): boolean {
        return this.currentStep === GameSetupStateService.STEP_GENERATE_RUNGS;
    }

    goNextStep(step: number): void {
        this.currentStep = step;
    }

    getCurrentStep(): number {
        return this.currentStep;
    }
}


