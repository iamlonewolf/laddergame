import { Component, ElementRef, ViewChild } from "@angular/core";
import { GameSetupService } from "../game-setup.service";

@Component({
  selector: "app-game-setup-rung",
  templateUrl: "./game-setup-rung.component.html",
  styleUrls: [
    "./game-setup-rung.component.scss"
  ]
})
export class GameSetupRungComponent {
    constructor(private gameSetupService: GameSetupService) {

    }

    getTotalPlayers(): number {
        return this.gameSetupService.getTotalPlayers();
    }
}
