import * as _ from "lodash";
import { Component, Input } from "@angular/core";
import { Emoji, EMOJIS } from "./emoji-constant";
@Component({
  selector: "app-emoji-picker",
  templateUrl: "./emoji-picker.component.html",
  styleUrls: [
    "./emoji-picker.component.scss"
  ]
})
export class EmojiPickerComponent {
  constructor() {

  }

  getEmojiList(): Emoji[] {
    return EMOJIS;
  }
}



