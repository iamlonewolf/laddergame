import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { EmojiPickerComponent } from "./emoji-picker.component";

@NgModule({
  declarations: [
    EmojiPickerComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
  ],
  exports: [
    EmojiPickerComponent
  ],
  providers: [
  ],
  bootstrap: [
    EmojiPickerComponent
  ]
})
export class EmojiPickerModule {}
