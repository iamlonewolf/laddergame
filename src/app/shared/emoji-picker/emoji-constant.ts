export const EMOJIS = [
  {
    emojiId: 1,
    character: "😀",
  },
  {
    emojiId: 2,
    character: "😁",
  }
  ,
  {
    emojiId: 3,
    character: "😂",
  },
  {
    emojiId: 4,
    character: "🤣",
  },
  {
    emojiId: 5,
    character: "😃",
  },
  {
    emojiId: 6,
    character: "😄",
  },
  {
    emojiId: 7,
    character: "😅",
  },
  {
    emojiId: 8,
    character: "😆",
  },
  {
    emojiId: 9,
    character: "😉",
  },
  {
    emojiId: 10,
    character: "😊",
  },
  {
    emojiId: 11,
    character: "😋",
  },
  {
    emojiId: 12,
    character: "😎",
  },
  {
    emojiId: 13,
    character: "😍",
  },
  {
    emojiId: 14,
    character: "😘",
  },
  {
    emojiId: 15,
    character: "🥰",
  },
  {
    emojiId: 16,
    character: "😗",
  },
  {
    emojiId: 17,
    character: "😙",
  },
  {
    emojiId: 18,
    character: "😚",
  },
  {
    emojiId: 19,
    character: "☺",
  },
  {
    emojiId: 20,
    character: "🙂",
  },
  {
    emojiId: 21,
    character: "🤗",
  },
  {
    emojiId: 22,
    character: "🤩",
  },
  {
    emojiId: 23,
    character: "🤔",
  },
  {
    emojiId: 24,
    character: "🤨",
  },
  {
    emojiId: 25,
    character: "😐",
  },
  {
    emojiId: 26,
    character: "😑",
  },
  {
    emojiId: 27,
    character: "😶",
  },
  {
    emojiId: 28,
    character: "🙄",
  }
];

export class Emoji {
  emojiId?: number;
  character?: string;
  url?: string;
}
