import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RungCellModule } from "./rung-cell/rung-cell.module";
import { RungGeneratorComponent } from "./rung-generator.component";
import { RungGeneratorService } from "./rung-generator.service";

@NgModule({
    imports: [
        NgbModule,
        CommonModule,
        FormsModule,
        RungCellModule,
    ],
    declarations: [
        RungGeneratorComponent
    ],
    exports: [
        RungGeneratorComponent
    ],
    providers: [
        RungGeneratorService
    ],
    bootstrap: [
        RungGeneratorComponent
    ]
})
export class RungGeneratorModule {}
