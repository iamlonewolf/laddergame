import * as _ from "lodash";
import { Injectable } from "@angular/core";
import { Observable, of, from } from "rxjs";
import { map } from "rxjs/operators";
import { RungContainer, generateRungs } from "./rung-util";
@Injectable()
export class RungGeneratorService {
    static readonly TOTAL_ROWS = 20;
    private currentList: RungContainer[];
    private totalPlayers: number = 2;
    constructor() {
    }

    getRungs(): RungContainer[] {
        return this.currentList;
    }

    getTotalPlayers(): number {
        return this.totalPlayers;
    }

    generateRungs(totalPlayers: number, totalRows: number = RungGeneratorService.TOTAL_ROWS): Observable<RungContainer[]> {
        this.totalPlayers = totalPlayers;
        return of(generateRungs(this.getTotalPlayers(), totalRows))
            .pipe(map((list) => {
                this.currentList = list;
                return list;
            }));
    }

    draw(): void {
        this.generateRungs(this.getTotalPlayers()).subscribe();
    }

}

















