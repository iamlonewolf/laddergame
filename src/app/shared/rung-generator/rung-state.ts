import * as _ from "lodash";
import { RungCell, MODE_ON, MODE_OFF, RungContainer } from "./rung-util";
import { RungDirection } from "./rung-direction";
import { Observable, interval, Subject, of } from "rxjs";
import { map, takeUntil, flatMap } from "rxjs/operators";

export class RungState {
    static readonly MOVE_INTERVAL = 300;
    static readonly MAX_WIDTH_HEIGHT = 100;
    static readonly COUNTER_OFFSET = 50;

    private rungs: RungContainer[] = [];
    private totalColumns: number;
    private totalRows: number;
    private indexList: RungDirection[] = [];
    private startingIndex = 0;
    private currentColumnIndex = 0;
    private currentRowIndex = 0;
    private color: string;

    constructor(rungs: RungContainer[]) {
        this.rungs = rungs;
        this.totalColumns = _.size(rungs);
        this.totalRows = _.size(_.get(_.head(rungs), "list"));
    }

    getRungs(): RungContainer[] {
        return this.rungs;
    }

    getTotalColumns(): number {
        return this.totalColumns;
    }

    getTotalRows(): number {
        return this.totalRows;
    }

    go(index: number): void {
        this.currentColumnIndex = index;
        this.currentRowIndex = 0;
        this.determineDirection();
    }

    setColor(color: string): void {
        this.color = color;
    }

    getColor(): string {
        return this.color;
    }

    getStartingIndex(): number {
        return this.startingIndex;
    }

    getRungDirectionInstance(): RungDirection {
        return _.find(this.indexList, (rungDirection: RungDirection) => {
            return this.getStartingIndex() === rungDirection.getRungIndex();
        });
    }

     moveLeft(): Observable<any> {
        let lengthCounter = 0;
        const currentCell = this.getCurrentCell();
        if (!currentCell) {
            return of(undefined);
        }
        currentCell.borderColorRight = this.getColor();
        const currentObservable = Observable.create((observable) => {
            const timeInterval = interval(RungState.MOVE_INTERVAL)
                .subscribe(() => {
                    if (lengthCounter <= RungState.MAX_WIDTH_HEIGHT) {
                        currentCell.right = lengthCounter;
                        lengthCounter += RungState.COUNTER_OFFSET;
                        return;
                    }
                    timeInterval.unsubscribe();
                    observable.next(true);
                    observable.complete();
                });
        });

        return currentObservable;
    }

    moveRight(): Observable<any> {
        let lengthCounter = 0;
        const currentCell = this.getCurrentCell();
        if (!currentCell) {
            return of(undefined);
        }
        currentCell.borderColorLeft = this.getColor();
        const currentObservable = Observable.create((observable) => {
            const timeInterval = interval(RungState.MOVE_INTERVAL)
                .subscribe(() => {
                    if (lengthCounter <= RungState.MAX_WIDTH_HEIGHT) {
                        currentCell.left = lengthCounter;
                        lengthCounter += RungState.COUNTER_OFFSET;
                        return;
                    }
                    timeInterval.unsubscribe();
                    observable.next(true);
                    observable.complete();
                });
        });
        return currentObservable;
    }

    moveLeftDown(): Observable<any> {
        let lengthCounter = 0;
        const currentCell = this.getCurrentCell();
        if (!currentCell) {
            return of(undefined);
        }
        currentCell.borderColorLeftBottom = this.getColor();

        const currentObservable = Observable.create((observable) => {
            const timeInterval = interval(RungState.MOVE_INTERVAL)
                .subscribe(() => {
                    if (lengthCounter <= RungState.MAX_WIDTH_HEIGHT) {
                        currentCell.leftBottom = lengthCounter;
                        lengthCounter += RungState.COUNTER_OFFSET;
                        return;
                    }
                    timeInterval.unsubscribe();
                    observable.next(true);
                    observable.complete();
                });
        });
        return currentObservable;
    }

    moveRightDown(): Observable<any> {
        let lengthCounter = 0;
        const currentCell = this.getCurrentCell();
        if (!currentCell) {
            return of(undefined);
        }
        currentCell.borderColorRightBottom = this.getColor();
        const currentObservable = Observable.create((observable) => {
            const timeInterval = interval(RungState.MOVE_INTERVAL)
                .subscribe(() => {
                    if (lengthCounter <= RungState.MAX_WIDTH_HEIGHT) {
                        currentCell.rightBottom = lengthCounter;
                        lengthCounter += RungState.COUNTER_OFFSET;
                        return;
                    }
                    timeInterval.unsubscribe();
                    observable.next(true);
                    observable.complete();
                });
        });
        return currentObservable;
    }

    private determineDirection(): void {
        if (this.currentRowIndex === this.getTotalRows()) {
            console.log("END GAME: ");
            return;
        }
        if (!this.hasRightCell() && !this.isCurrentCellOn()) {
            this.decrementColumnIndex();
            if (this.isCurrentCellOn()) {
                this.moveRightDown()
                    .pipe(flatMap(() => {
                        return this.moveLeft();
                    }))
                    .subscribe(() => {
                        this.incrementRowIndex();
                        this.determineDirection();
                    });

                return;
            }

            this.moveRightDown()
                .subscribe(() => {
                    this.incrementColumnIndex();
                    this.incrementRowIndex();
                    this.determineDirection();
                });
            return;
        }

        if (!this.hasLeftCell() && this.isCurrentCellOn()) {
            this.moveLeftDown()
                .pipe(flatMap(() => {
                    return this.moveRight();
                }))
                .subscribe(() => {
                    this.incrementColumnIndex();
                    this.incrementRowIndex();
                    this.determineDirection();
                });
            return;
        }

        if (!this.isLeftCellOn() && !this.isCurrentCellOn()) {
            this.moveLeftDown()
                .subscribe(() => {
                    this.incrementRowIndex();
                    this.determineDirection();
                });
            return;
        }

        if (!this.isLeftCellOn() && this.isCurrentCellOn()) {
            this.moveLeftDown()
                .pipe(flatMap(() => {
                    return this.moveRight();
                }))
                .subscribe(() => {
                    this.incrementColumnIndex();
                    this.incrementRowIndex();
                    this.determineDirection();
                });
            return;
        }

        if (this.isLeftCellOn() && !this.isCurrentCellOn()) {
            this.decrementColumnIndex();
            this.moveRightDown()
                .pipe(flatMap(() => {
                    return this.moveLeft();
                }))
                .subscribe(() => {
                    this.incrementRowIndex();
                    this.determineDirection();
                })
            return;
        }

        if (!this.hasRightCell()) {
            this.moveRightDown()
                .subscribe(() => {
                    this.incrementRowIndex();
                    this.determineDirection();
                });
        }
    }

    getCurrentCell(): RungCell {
        const currentColumns = this.getCurrentColumnsByIndex(this.getCurrentColumnIndex());
        return !_.isUndefined(currentColumns) ? currentColumns[this.getCurrentRowIndex()] : undefined;
    }

    getLeftCell(): RungCell {
        const currentColumns = this.getCurrentColumnsByIndex(this.getCurrentColumnIndex() - 1);
        return !_.isUndefined(currentColumns) ? currentColumns[this.getCurrentRowIndex()] : undefined;
    }

    getRightCell(): RungCell {
        const currentColumns = this.getCurrentColumnsByIndex(this.getCurrentColumnIndex() + 1);
        return !_.isUndefined(currentColumns) ? currentColumns[this.getCurrentRowIndex()] : undefined;
    }

    getCurrentColumnsByIndex(index: number): RungCell[] {
        const currentColumns = this.getRungs()[index];
        return !_.isUndefined(currentColumns) ? _.get(currentColumns, "list") : undefined;
    }

    hasCurrentCell(): boolean {
        return !_.isUndefined(this.getCurrentCell());
    }

    hasLeftCell(): boolean {
        return !_.isUndefined(this.getLeftCell());
    }

    hasRightCell(): boolean {
        return !_.isUndefined(this.getRightCell());
    }

    isCurrentCellOn(): boolean {
        return _.get(this.getCurrentCell(), "mode") === MODE_ON;
    }

    isLeftCellOn(): boolean {
        return this.hasLeftCell() && _.get(this.getLeftCell(), "mode") === MODE_ON;
    }

    isRigtCellOn(): boolean {
        return this.hasRightCell() && _.get(this.getRightCell(), "mode") === MODE_ON;
    }

    getCurrentRowIndex(): number {
        return this.currentRowIndex;
    }

    getCurrentColumnIndex(): number {
        return this.currentColumnIndex;
    }

    incrementRowIndex(): void {
        this.currentRowIndex += 1;
    }

    decrementRowIndex(): void {
        this.currentRowIndex -= 1;
    }

    incrementColumnIndex(): void {
        this.currentColumnIndex += 1;
    }

    decrementColumnIndex(): void {
        this.currentColumnIndex -= 1;
    }

    reset(): void {
    }
}






