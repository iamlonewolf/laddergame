import * as _ from "lodash";

export const MODE_OFF = 0;
export const MODE_ON = 1;

export const generateRungs = (totalPlayer: number, totalRows: number): RungContainer[] => {
    const columnRange = _.range(0, totalPlayer - 1);
    const RUNG_THRESHOLD = 3;
    const FIRST_ROW = 0;
    const COUNTER_RESET_VALUE = 0;

    const nullRungs = _.map(_.range(0, totalRows), () => {
        const rung = new RungCell();
        rung.disabled = true;
        return rung;
    });
    const disableRungContainer = new RungContainer();
    disableRungContainer.disabled = true;
    disableRungContainer.list = nullRungs;

    const rungList: RungContainer[] = _.reduce(columnRange, (acc: RungContainer[], colKey: number) => {
        let offStateCounter = COUNTER_RESET_VALUE;
        let onStateCounter = COUNTER_RESET_VALUE;
        const rowRange = _.range(0, totalRows);
        const rungContainer = new RungContainer();
        const rungs = _.map(rowRange, (rowKey: number) => {
            const rungCell = new RungCell();
            const mode = getRandomMode();
            rungCell.row = rowKey;
            rungCell.column = colKey;

            rungCell.borderColorLeft = undefined;
            rungCell.borderColorRight = undefined;
            rungCell.borderColorLeftBottom = undefined;
            rungCell.borderColorRightBottom = undefined;

            if ((totalRows - 1) === rowKey) {
                rungCell.mode = MODE_OFF;
                return rungCell;
            }
            if (mode === MODE_OFF) {
                offStateCounter += 1;
            }

            if (mode === MODE_ON) {
                onStateCounter += 1;
            }

            if (onStateCounter === RUNG_THRESHOLD) {
                onStateCounter = COUNTER_RESET_VALUE;
                rungCell.mode = MODE_OFF;
                return rungCell;
            }

            if (colKey === FIRST_ROW) {
                rungCell.mode = mode;
                return rungCell;
            }

            if (acc[colKey - 1].list[rowKey].mode === MODE_ON) {
                rungCell.mode = MODE_OFF;
                return rungCell;
            }

            if (offStateCounter === RUNG_THRESHOLD) {
                offStateCounter = COUNTER_RESET_VALUE;
                rungCell.mode = MODE_ON;
                return rungCell;
            }
            rungCell.mode = mode;
            return rungCell;
        });

        rungContainer.list = rungs;
        acc.push(rungContainer);
        return acc;
    }, []);

    rungList.push(disableRungContainer);
    return rungList;
};

export const getRandomMode = (): number => {
    const MIN_RANGE = 0;
    const MAX_RANGE = 1;
    return _.random(MIN_RANGE, MAX_RANGE);
};

export class RungContainer {
    disabled?: boolean = false;
    list?: RungCell[];
};

export class RungCell {
    mode?: number = 0;
    left?: number = 0;
    right?: number = 0;
    leftBottom?: number = 0;
    rightBottom?: number = 0;
    column?: number = 0;
    row?: number = 0;
    disabled?: boolean = false;
    borderColorLeft?: string = undefined;
    borderColorRight?: string = undefined;
    borderColorRightBottom?: string = undefined;
    borderColorLeftBottom?: string = undefined;
}












