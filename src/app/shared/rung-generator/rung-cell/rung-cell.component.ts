import * as _ from "lodash";
import { Component, Input, OnInit } from "@angular/core";
import { RungCell } from "../rung-util";


@Component({
    selector: "app-rung-cell",
    templateUrl: "./rung-cell.component.html",
    styleUrls: [
        "./rung-cell.component.scss"
    ]
})
export class RungCellComponent implements OnInit {
    @Input() cell: RungCell;
    @Input() showLabel: boolean = false;
    constructor() {

    }

    private initialize(): void {

    }

    getCell(): RungCell {
        return this.cell;
    }

    isOn(): boolean {
        return _.get(this.getCell(), "mode");
    }

    getRow(): number {
        return _.get(this.getCell(), "row");
    }

    getColumn(): number {
        return _.get(this.getCell(), "column");
    }

    getLeftWidth(): string {
        return this.cell.left + "%";
    }

    getRightWidth(): string {
        return this.cell.right + "%";
    }

    borderRightColor(): string {
        return _.get(this.getCell(), "borderColorRight",  _.get(this.getCell(), "borderColorRightBottom"));
    }

    borderLeftColor(): string {
        return _.get(this.getCell(), "borderColorLeft", _.get(this.getCell(), "borderColorLeftBottom"));
    }

    getLeftHeight(): string {
        return this.cell.leftBottom + "%";
    }

    getRightHeight(): string {
        return this.cell.rightBottom + "%";
    }

    showLeft(): boolean {
        return this.cell.leftBottom > 0;
    }

    showRight(): boolean {
        return this.cell.rightBottom > 0;
    }

    isShowLabel(): boolean {
        return this.showLabel;
    }

    ngOnInit() {
        this.initialize();
    }
}





