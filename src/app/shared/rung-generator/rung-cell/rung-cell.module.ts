import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RungCellComponent } from "./rung-cell.component";

@NgModule({
    imports: [
        NgbModule,
        CommonModule,
        FormsModule,
    ],
    declarations: [
        RungCellComponent
    ],
    exports: [
        RungCellComponent
    ],
    bootstrap: [
        RungCellComponent
    ]
})
export class RungCellModule {}
