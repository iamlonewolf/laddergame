import * as _ from "lodash";
import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges } from "@angular/core";
import {
    RungCell, RungContainer, MODE_ON
} from "./rung-util";
import { Player } from "../../model/types/player";
import { RungGeneratorService } from "./rung-generator.service";
import { RungState } from "./rung-state";
import { interval, of } from "rxjs";
import { takeUntil } from "rxjs/operators";
@Component({
    selector: "app-rung-generator",
    templateUrl: "./rung-generator.component.html",
    styleUrls: [
        "./rung-generator.component.scss"
    ]
})
export class RungGeneratorComponent implements OnInit, OnChanges, OnDestroy {
    @Input() players: Player[] = [];
    @Input() totalPlayers: number = 2;
    private currentList: RungContainer[];
    constructor(private rungGeneratorService: RungGeneratorService) {
    }

    private initialize(): void {
        const TOTAL_ROWS = 20;
        this.rungGeneratorService.generateRungs(this.getTotalPlayers(), TOTAL_ROWS)
            .subscribe((list: RungContainer[]) => {
                this.currentList = list;
                console.log("currentList", this.currentList);
                const s1 = new RungState(this.currentList);
                const s2 = new RungState(this.currentList);
                const s3 = new RungState(this.currentList);
                const s4 = new RungState(this.currentList);
                const s5 = new RungState(this.currentList);
                const s6 = new RungState(this.currentList);
                const s7 = new RungState(this.currentList);
                const s8 = new RungState(this.currentList);
                const s9 = new RungState(this.currentList);
                const s10 = new RungState(this.currentList);

                s1.setColor("#1444cc");
                s1.go(0);

                s2.setColor("#2ce40d");
                s2.go(1);

                s3.setColor("#f70825");
                s3.go(3);

                s4.setColor("#e2d878");
                s4.go(4);

                s5.setColor("#ce26b4");
                s5.go(8);

                s6.setColor("#fb0beb");
                s6.go(10);

                s7.setColor("#fffe15");
                s7.go(12);

                s8.setColor("#fed8ff");
                s8.go(13);

                s9.setColor("#d8ffd8");
                s9.go(6);

                s10.setColor("#0d33bc");
                s10.go(7);
        });
    }

    isOn(row: number): boolean {
        return row === MODE_ON;
    }

    getList(): RungContainer[] {
        return this.rungGeneratorService.getRungs();
    }

    getTotalPlayers(): number {
        return 16;
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (_.has(changes, "totalPlayers")
            && changes.totalPlayers.currentValue !== changes.totalPlayers.previousValue) {
            this.initialize();
        }
    }

    ngOnDestroy() {
    }
}





