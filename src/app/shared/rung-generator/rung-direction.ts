import { RungContainer } from "./rung-util";

export class RungDirection {
    private rungIndex: number;
    private currentColumnIndex = 0;
    private currentRowIndex = 0;

    getCurrentRowIndex(): number {
        return this.currentRowIndex;
    }

    getCurrentColumnIndex(): number {
        return this.currentColumnIndex;
    }

    incrementRowIndex(): void {
        this.currentRowIndex += 1;
    }

    decrementRowIndex(): void {
        this.currentRowIndex -= 1;
    }

    incrementColumnIndex(): void {
        this.currentColumnIndex += 1;
    }

    decrementColumnIndex(): void {
        this.currentColumnIndex -= 1;
    }

    setRungIndex(index: number): void {
        this.rungIndex = index;
    }

    getRungIndex(): number {
        return this.rungIndex;
    }
}

