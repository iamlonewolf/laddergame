import { NgModule } from "@angular/core";
import { AppService } from "./app.service";

@NgModule({
    declarations: [
        AppService
    ],
    imports: [

    ],
    providers: [
        AppService
    ],
})
export class AppModule { }
