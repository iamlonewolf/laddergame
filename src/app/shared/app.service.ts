import { Injectable } from "@angular/core";

@Injectable()
export class AppService {
  static readonly STEP_ONE: number = 1;
  static readonly STEP_TWO: number = 2;
  static readonly STEP_THREE: number = 2;
  private currentStep: number = AppService.STEP_ONE;

    constructor() {
    }

    goNext(): void {
        if (this.currentStep < 3) {
            this.currentStep += 1;
        }
    }

    goBack(): void {
        if (this.currentStep > 0) {
            this.currentStep -= 1;
        }
    }

    isStepOne(): boolean {
        return this.currentStep === AppService.STEP_ONE;
    }

    isStepTwo(): boolean {
        return this.currentStep === AppService.STEP_TWO;
    }

    isStepThree(): boolean {
        return this.currentStep === AppService.STEP_THREE;
    }

    getCurrentStep(): number {
        return this.currentStep;
    }
}
