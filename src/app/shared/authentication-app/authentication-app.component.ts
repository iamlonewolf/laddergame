import { Component, ElementRef, ViewChild } from "@angular/core";

@Component({
  selector: "app-authentication-app",
  templateUrl: "./authentication-app.component.html",
  styleUrls: [
    "./authentication-app.component.scss"
  ]
})
export class AuthenticationAppComponent {
  constructor() {
  }
}
