import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationAppComponent } from "./authentication-app.component";
import { AuthenticationLoginAppComponent } from "./authentication-login-app/authentication-login-app.component";
import { AuthenticationRegisterAppComponent } from "./authentication-register-app/authentication-register-app.component";
import { AuthenticationAppService } from "./authentication-app.service";
import { AuthenticationAdapterAppService } from "./authentication-adapter-app-service";
import { AuthenticationLoginAppService } from "./authentication-login-app.service";
import { AuthenticationRegisterAppService } from "./authentication-register-app.service";

@NgModule({
    imports: [
        NgbModule,
        FormsModule
    ],
    declarations: [
        AuthenticationAppComponent,
        AuthenticationLoginAppComponent,
        AuthenticationRegisterAppComponent,
    ],
    exports: [
        AuthenticationAppComponent,
        AuthenticationLoginAppComponent,
        AuthenticationRegisterAppComponent,
    ],
    providers: [
        AuthenticationAppService,
        AuthenticationAdapterAppService,
        AuthenticationLoginAppService,
        AuthenticationRegisterAppService
    ]
})
export class AuthenticationAppModule {}
