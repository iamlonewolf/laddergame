import { Observable } from "rxjs";

export interface AuthenticationInterface {
    postAuthenticate(parameters: object): Observable<any>;
}

