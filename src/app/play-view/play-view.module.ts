import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RungGeneratorModule } from "../shared/rung-generator/rung-generator.module";
import { PlayViewRouteModule } from "./play-view-route.module";
import { PlayViewComponent } from "./play-view.component";

@NgModule({
  imports: [
    NgbModule,
    RungGeneratorModule,
    PlayViewRouteModule,
  ],
  declarations: [
    PlayViewComponent
  ],
  providers: [
  ],
})
export class PlayViewModule {}
