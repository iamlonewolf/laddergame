import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PlayViewComponent } from "./play-view.component";
const routes: Routes = [
  {
    path: "",
    component: PlayViewComponent
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
})
export class PlayViewRouteModule {}
