import { Component, ElementRef, ViewChild } from "@angular/core";

@Component({
  selector: "app-stub",
  templateUrl: "./stub.component.html",
  styleUrls: [
    "./stub.component.scss"
  ]
})
export class StubComponent {
  constructor() {
  }
}
