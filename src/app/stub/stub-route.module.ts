import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StubComponent } from "./stub.component";
const routes: Routes = [
  {
    path: "",
    component: StubComponent
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
})
export class StubRouteModule {}
