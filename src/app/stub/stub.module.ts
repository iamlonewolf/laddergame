import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { StubRouteModule } from "./stub-route.module";
import { StubComponent } from "./stub.component";

@NgModule({
  declarations: [
    StubComponent
  ],
  imports: [
    NgbModule,
    FormsModule,
    StubRouteModule
  ],
  providers: [
  ],
})
export class StubModule {}
