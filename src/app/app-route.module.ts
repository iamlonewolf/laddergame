import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {
        path: "",
        loadChildren: "./about-view/about-view.module#AboutViewModule"
    },
    {
        path: "about",
        loadChildren: "./about-view/about-view.module#AboutViewModule"
    },
    {
        path: "setup",
        loadChildren: "./setup-view/setup-view.module#SetupViewModule"
    },
    {
        path: "play",
        loadChildren: "./play-view/play-view.module#PlayViewModule"
    },
    {
        path: "game",
        loadChildren: "./game-view/game-view.module#GameViewModule"
    },
    {
        path: "purchase",
        loadChildren: "./purchase-view/purchase-view.module#PurchaseViewModule"
    },
    {
        path: "login",
        loadChildren: "./authentication-view/authentication-view.module#AuthenticationViewModule"
    },
    {
        path: "register",
        loadChildren: "./authentication-view/authentication-view.module#AuthenticationViewModule"
    }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
})
export class AppRouteModule {}
