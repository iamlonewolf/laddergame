import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RungGeneratorModule } from "../shared/rung-generator/rung-generator.module";
import { GameViewRouteModule } from "./game-view-route.module";
import { GameViewComponent } from "./game-view.component";

@NgModule({
  declarations: [
    GameViewComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    RungGeneratorModule,
    GameViewRouteModule,
  ],
  providers: [
  ],
})
export class GameViewModule {}
