import { Component, ElementRef, ViewChild } from "@angular/core";

@Component({
  selector: "app-game-view",
  templateUrl: "./game-view.component.html",
  styleUrls: [
    "./game-view.component.scss"
  ]
})
export class GameViewComponent {

  constructor() {
  }

  getColumns(): any[] {
    return [
      {
        id: 1,
      },
      {
        id: 2,
      },
      {
        id: 3,
      }
    ];
  }

  getRows(): any[] {
    return [
      {
        id: 1,
      },
      {
        id: 2,
      },
      {
        id: 3,
      },
      {
        id: 4,
      }
    ];
  }

  trackBy(index): number {
    return index;
  }
}


