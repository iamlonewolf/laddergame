import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { AppRouteModule } from "./app-route.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRouteModule,
  ],
  exports: [
    BrowserModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
