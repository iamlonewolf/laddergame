import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ColorPickerModule } from "ngx-color-picker";
import { GameSetupModule } from "../shared/game-setup/game-setup.module";
import { SetupViewRouteModule } from "./setup-view-route.module";
import { SetupViewComponent } from "./setup-view.component";

@NgModule({
    declarations: [
        SetupViewComponent
    ],
    imports: [
        NgbModule,
        FormsModule,
        ColorPickerModule,
        GameSetupModule,
        SetupViewRouteModule
    ],
    providers: [
    ],
})
export class SetupViewModule {}
