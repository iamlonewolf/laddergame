import { Component, ElementRef, ViewChild } from "@angular/core";

@Component({
  selector: "app-setup-view",
  templateUrl: "./setup-view.component.html",
  styleUrls: [
    "./setup-view.component.scss"
  ]
})
export class SetupViewComponent {

  color: string;
  constructor() {
  }

}
