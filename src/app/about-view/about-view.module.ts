import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AboutViewRouteModule } from "./about-view-route.module";

@NgModule({
  declarations: [
  ],
  imports: [
    NgbModule,
    FormsModule,
    AboutViewRouteModule
  ],
  providers: [
  ],
})
export class AboutViewModule {}
