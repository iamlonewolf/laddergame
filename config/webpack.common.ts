const path = require("path");
const AssetsPlugin = require("assets-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const assetsPluginInstance = new AssetsPlugin({
  filename: "assets.bundle.json",
  path: path.resolve(__dirname, "../src/dist")
});

module.exports = {
    entry: {
        vendor: "./src/vendor.ts",
        polyfills: "./src/polyfills.ts",
        main: "./src/main.ts",
    },
    output: {
        filename: "[name].[contentHash].bundle.js",
        chunkFilename: '[name].[chunkhash].bundle.js',
        path: path.resolve(__dirname, "../src/dist"),
        publicPath: "/"
    },
    devtool: "eval-source-map",
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            cacheGroups: {
            vendor: {
                test: /[\\/]node_modules[\\/]/,
                name(module) {
                // get the name. E.g. node_modules/packageName/not/this/part.js
                // or node_modules/packageName
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                // npm package names are URL-safe, but some servers don't like @ symbols
                return `package.${packageName.replace('@', '')}`;
                },
            },
            },
        },
    },
    plugins: [
        new CompressionPlugin(),
        new OptimizeCssAssetsPlugin(),
        new CleanWebpackPlugin(),
        assetsPluginInstance,
        new MiniCssExtractPlugin({ filename: "[name].[contentHash].css" }),
        new HtmlWebpackPlugin({
            template: "./src/template.html"
        })
    ],
    module: {
    rules: [
        {
            test: /\.(css|scss)$/,
            use: [
                "to-string-loader",
                "style-loader",
                MiniCssExtractPlugin.loader,
                "css-loader",
                "sass-loader",
            ],
        },
        {
            test: /\.ts$/,
            use: [
                "ts-loader",
                "angular2-template-loader"
            ],
            exclude: /node_modules/
        },
        {
            test: /\.html$/,
            use: [
                "html-loader"
            ],
            },
        {
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            use: [
                {
                loader: "file-loader"
                }
            ]
        },
        {
            test: /\.(ts|js)$/,
            loaders: [
                "angular-router-loader"
            ]
        }
    ]
    },
    resolve: {
        extensions: [
            ".ts",
            ".js",
            ".json",
        ]
    }
};
