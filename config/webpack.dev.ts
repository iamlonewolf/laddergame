export {};
const common = require("./webpack.common");
const merge = require("webpack-merge");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const compression = require('compression');

module.exports = merge(common, {
    mode: "development",
    watch: false,
    devServer: {
        port: 9000,
        compress: true,
        historyApiFallback: true
    },
    plugins: [
    // new BundleAnalyzerPlugin(),
    ]
});
