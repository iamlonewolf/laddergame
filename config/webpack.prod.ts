export {};
const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const BrotliPlugin = require("brotli-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = merge(common, {
    mode: "production",
    output: {
        path: path.resolve(__dirname, "../../../laddergame.johnsikstri.com/public_html"),
    },
    devtool: "",
    plugins: [
        // new BrotliPlugin({
        //     asset: '[path].br[query]',
        //     test: /\.(js|css|html|svg)$/,
        //     threshold: 10240,
        //     minRatio: 0.8
        // }),
    ],
    optimization: {
        minimizer: [
            // new TerserPlugin(),
            new UglifyJsPlugin()
        ],
    }
});
